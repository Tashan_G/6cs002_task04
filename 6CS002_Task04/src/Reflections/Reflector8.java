package Reflections;

import java.lang.reflect.Field;

public class Reflector8 {
    public static void main(String[] args) throws Exception {
        SimpleFloat simpleFloat = new SimpleFloat();
        Field[] fields = simpleFloat.getClass().getDeclaredFields();
        System.out.println("Total field count: " + fields.length);
        for (Field f : fields) {
            f.setAccessible(true);
            float x = (float) f.get(simpleFloat);
            x += 1.5; // Example modification
            f.set(simpleFloat, x);
            System.out.printf("field name-%s type-%s value-%s\n", f.getName(), f.getType(), f.get(simpleFloat));
        }
    }
}
