package Reflections;

import java.lang.reflect.Method;

public class Reflector9 {
    public static void main(String[] args) throws Exception {
        SimpleFloat simpleFloat = new SimpleFloat();
        Method[] methods = simpleFloat.getClass().getMethods();
        System.out.println("Total method count: " + methods.length);
        for (Method method : methods) {
            System.out.printf("Method name-%s type-%s parameters:", method.getName(), method.getReturnType());
            Class<?>[] types = method.getParameterTypes();
            for (Class<?> type : types) {
                System.out.print(type.getName() + " ");
            }
            System.out.println();
        }
    }
}
