package Reflections;

public class Reflector3 {
    public static void main(String[] args) {
        SimpleFloat simpleFloat = new SimpleFloat();
        System.out.println("class: " + simpleFloat.getClass());
        System.out.println("class name: " + simpleFloat.getClass().getName());
    }
}
