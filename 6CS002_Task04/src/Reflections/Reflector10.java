package Reflections;

import java.lang.reflect.Method;

public class Reflector10 {
    public static void main(String[] args) throws Exception {
        SimpleFloat simpleFloat = new SimpleFloat();
        Method m = simpleFloat.getClass().getDeclaredMethod("setX", float.class);
        m.setAccessible(true);
        m.invoke(simpleFloat, 7.5); // Example modification
        System.out.println(simpleFloat);
    }
}
