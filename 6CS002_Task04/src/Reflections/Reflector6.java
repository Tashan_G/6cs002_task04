package Reflections;

import java.lang.reflect.Field;

public class Reflector6 {
    public static void main(String[] args) throws Exception {
        SimpleFloat simpleFloat = new SimpleFloat();
        Field[] fields = simpleFloat.getClass().getDeclaredFields();
        System.out.println("Total field count: " + fields.length);
        for (Field f : fields) {
            System.out.printf("field name-%s type-%s accessible-%s\n", f.getName(), f.getType(), f.isAccessible());
        }
    }
}
