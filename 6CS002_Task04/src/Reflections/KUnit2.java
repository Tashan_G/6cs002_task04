package Reflections;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class KUnit2 {
    private static final String className = KUnit2.class.getName();
    private static Logger logger = Logger.getLogger(className);

    private static List<String> checks;
    private static float checksMade = 0;
    private static float passedChecks = 0;
    private static float failedChecks = 0;

    private static void addToReport(String txt) {
        if (checks == null) {
            checks = new LinkedList<String>();
        }
        checks.add(String.format("%04d: %s", checksMade++, txt));
    }

    public static void checkEquals(String value1, String value2) {
        if (value1.equals(value2)) {
            addToReport(String.format("Test PASS - The value %s is identical to value %s", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("Test FAIL - The value %s is not identical to value %s", value1, value2));
            failedChecks++;
            logger.warning(String.format("Test FAIL - The value %s is not identical to value %s", value1, value2));
        }
    }

    public static void checkNotEquals(String value1, String value2) {
        if (!value1.equals(value2)) {
            addToReport(String.format("Test PASS - The value %s is not identical to value %s", value1, value2));
            passedChecks++;
        } else {
            addToReport(String.format("Test FAIL - The value %s is identical to value %s", value1, value2));
            failedChecks++;
            logger.warning(String.format("Test FAIL - The value %s is identical to value %s", value1, value2));
        }
    }

    public static void checkComplexAssertion(Object object, String methodName, Object[] args, Object expectedValue) {
        try {
            Method method = object.getClass().getDeclaredMethod(methodName, getParameterTypes(args));
            method.setAccessible(true);
            Object result = method.invoke(object, args);

            if (result.equals(expectedValue)) {
                addToReport(String.format("Test PASS - Method %s returned the expected value", methodName));
                passedChecks++;
            } else {
                addToReport(String.format("Test FAIL - Method %s did not return the expected value", methodName));
                failedChecks++;
                logger.warning(String.format("Test FAIL - Method %s did not return the expected value", methodName));
            }
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            handleException(e);
        }
    }

    private static Class<?>[] getParameterTypes(Object[] args) {
        Class<?>[] parameterTypes = new Class<?>[args.length];
        for (int i = 0; i < args.length; i++) {
            parameterTypes[i] = args[i].getClass();
        }
        return parameterTypes;
    }

    private static void handleException(Exception e) {
        addToReport(String.format("Exception occurred: %s", e.getMessage()));
        logger.severe(String.format("Exception occurred: %s", e.getMessage()));
        failedChecks++;
    }

    public static void report() {
        Logger parent = logger.getParent();

        Handler[] parentHandlers = parent.getHandlers();
        for (Handler handler : parentHandlers) {
            System.out.println("removing " + handler);
            parent.removeHandler(handler);
        }

        Handler consoleHandler = new ConsoleHandler();
        consoleHandler.setLevel(Level.WARNING);
        logger.addHandler(consoleHandler);

        logger.info("message");

        System.out.printf("%d checks passed\n", passedChecks);
        System.out.printf("%d checks failed\n", failedChecks);
        System.out.println();

        for (String check : checks) {
            System.out.println(check);
        }
    }
}
