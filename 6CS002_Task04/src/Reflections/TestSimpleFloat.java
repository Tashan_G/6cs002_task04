package Reflections;

import static Reflections.KUnit2.*;

/******************************************************************************
 * This code demonstrates the use of the KUnit2 testing tool. It produces a
 * report that contains messages generated from the check methods.
 * 
 * @author Dr Kevan Buckley, University of Wolverhampton, 2019
 ******************************************************************************/

public class TestSimpleFloat {

    void checkConstructorAndAccess() {
        SimpleFloat SF = new SimpleFloat(5.2f, 3.7f);
        checkEquals(Float.toString(SF.getX()), "5.2");
        checkEquals(Float.toString(SF.getY()), "3.7");
        checkNotEquals(Float.toString(SF.getY()), "6.2");
        checkNotEquals(Float.toString(SF.getY()), "4.1");
    }

    void checkSquareX() {
        SimpleFloat SF = new SimpleFloat(5.2f, 3.7f);
        SF.squareX();
        checkEquals(Float.toString(SF.getX()), "15.24");
    }

    public static void main(String[] args) {
        TestSimpleFloat TSF = new TestSimpleFloat();
        TSF.checkConstructorAndAccess();
        TSF.checkSquareX();
        report();
    }
}
