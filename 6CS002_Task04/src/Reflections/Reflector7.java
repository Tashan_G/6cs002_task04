package Reflections;

import java.lang.reflect.Field;

public class Reflector7 {
    public static void main(String[] args) throws Exception {
        SimpleFloat simpleFloat = new SimpleFloat();
        Field[] fields = simpleFloat.getClass().getDeclaredFields();
        System.out.println("Total field count: " + fields.length);
        for (Field f : fields) {
            f.setAccessible(true);
            System.out.printf("field name-%s type-%s value-%s\n", f.getName(), f.getType(), f.get(simpleFloat));
        }
    }
}
