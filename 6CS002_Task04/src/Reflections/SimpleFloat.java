package Reflections;

public class SimpleFloat {

  public float x = 10.2f;
  private float y = 20.5f;
  
  public SimpleFloat() {
  }
  
  public SimpleFloat(float x, float y) {
    this.x = x;
    this.y = y;
  }

  public void squareX(){
    this.x *= this.x;
  }

  private void squareY(){
    this.y *= this.y;
  }

  public float getX() {
    return x;
  }

  private void setX(float x) {
    this.x = x;
  }

  public float getY() {
    return y;
  }

  public void setY(float y) {
    this.y = y;
  }
  public void floatX() {
      this.x = this.x + this.x;
   }
  
  public String toString() {
    return String.format("(x:%.2f, y:%.2f)", x, y);
  }

}
